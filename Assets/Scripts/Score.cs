﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Score : MonoBehaviour
{
    public TextMeshProUGUI scoreText;
    public static int scorevalue;

    // Start is called before the first frame update
    void Start()
    {
        scorevalue = 0;
        UpdateScore(0);
        
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void UpdateScore(int scoreToAdd)
    {
        scorevalue += scoreToAdd;
        scoreText.text = "Score:" + scorevalue;
    }


}
