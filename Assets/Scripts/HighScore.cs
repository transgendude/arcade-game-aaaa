﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class HighScore : MonoBehaviour
{
    public static int highScore = 0;
    public TextMeshProUGUI highScoreText;
    public TextMeshProUGUI newHighScore;

    // Start is called before the first frame update
    void Start()
    {
        highScore = PlayerPrefs.GetInt("highScore", 0);        
        GetHighScore();
        PlayerPrefs.SetInt("highScore", highScore);
        PlayerPrefs.Save();
    }

    // Update is called once per frame
    void Update()
    {
        highScoreText.text = "High Score:" + highScore;
    }

    void GetHighScore()
    {
        if (Score.scorevalue > highScore)
        {
            Debug.Log("test");
            highScore = Score.scorevalue;
            newHighScore.gameObject.SetActive(true);     
        }

    }
}
