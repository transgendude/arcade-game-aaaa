﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SpawnManager : MonoBehaviour
{
    public GameObject weakEnemy;
    public GameObject strongEnemy;
    public float startDelay = 1;
    public Transform[] rightWaypoints;
    public Transform[] leftWaypoints;


    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(Spawner());
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    IEnumerator Spawner()
    {

        yield return new WaitForSeconds(2);

        SpawnLeftWeak();

        yield return new WaitForSeconds(1);

        SpawnRightWeak();

        yield return new WaitForSeconds(3);

        SpawnRightWeak();
        SpawnLeftWeak();

        yield return new WaitForSeconds(2);

        SpawnLeftWeak();
        SpawnRightWeak();

        yield return new WaitForSeconds(1);

        SpawnLeftStrong();

        yield return new WaitForSeconds(2);

        SpawnRightStrong();

        yield return new WaitForSeconds(2);

        SpawnLeftWeak();
        SpawnRightWeak();

        yield return new WaitForSeconds(1);

        SpawnLeftWeak();
        SpawnRightWeak();

        yield return new WaitForSeconds(1);

        SpawnLeftWeak();
        SpawnRightWeak();

        yield return new WaitForSeconds(1);

        SpawnLeftWeak();
        SpawnRightWeak();

        yield return new WaitForSeconds(1);

        SpawnLeftWeak();
        SpawnRightWeak();

        yield return new WaitForSeconds(2);

        SpawnLeftStrong();

        yield return new WaitForSeconds(1);

        SpawnLeftStrong();

        yield return new WaitForSeconds(2);

        SpawnRightStrong();

        yield return new WaitForSeconds(1);

        SpawnRightStrong();

        yield return new WaitForSeconds(3);

        SpawnLeftWeak();
        SpawnRightStrong();

        yield return new WaitForSeconds(2);

        SpawnLeftStrong();
        SpawnRightWeak();

        yield return new WaitForSeconds(3);
        SceneManager.LoadScene("Level Win");

    }


    void SpawnLeftWeak()
    {
        Vector3 leftSpawnPos = new Vector3(-3, 1, 0);

        GameObject newEnemy = Instantiate(weakEnemy, leftSpawnPos, weakEnemy.transform.rotation);
        newEnemy.GetComponent<FollowPath>().waypoints = leftWaypoints;
    } 

    void SpawnRightWeak()
    {
        Vector3 rightSpawnPos = new Vector3(3, 1, 0);

        GameObject newEnemy2 = Instantiate(weakEnemy, rightSpawnPos, weakEnemy.transform.rotation);
        newEnemy2.GetComponent<FollowPath>().waypoints = rightWaypoints;
    }

    void SpawnLeftStrong()
    {
        Vector3 leftSpawnPos = new Vector3(-3, 1, 0);

        GameObject newEnemy3 = Instantiate(strongEnemy, leftSpawnPos, strongEnemy.transform.rotation);
        newEnemy3.GetComponent<FollowPath>().waypoints = leftWaypoints;
    }

    void SpawnRightStrong()
    {
        Vector3 rightSpawnPos = new Vector3(3, 1, 0);

        GameObject newEnemy3 = Instantiate(strongEnemy, rightSpawnPos, strongEnemy.transform.rotation);
        newEnemy3.GetComponent<FollowPath>().waypoints = rightWaypoints;
    }


}
