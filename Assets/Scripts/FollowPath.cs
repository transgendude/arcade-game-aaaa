﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowPath : MonoBehaviour
{
    // Variables
    public Transform[] waypoints;
    public float moveSpeed = 2f;
    private int waypointIndex = 0;

    // Use this for initialization
    void Start()
    {
        // Set enemy position to first node
        transform.position = waypoints[waypointIndex].transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        // Call movement function
        Move();
    }

    void Move()
    {
        // Moves enemy until it is at the final node
        if (waypointIndex <= waypoints.Length - 1)
        {
            transform.position = Vector3.MoveTowards(transform.position,
               waypoints[waypointIndex].transform.position,
               moveSpeed * Time.deltaTime);

            // Moves the enemy to the next node once it reaches one
            if (transform.position == waypoints[waypointIndex].transform.position)
            {
                waypointIndex += 1;
            }
        }
    }
}
