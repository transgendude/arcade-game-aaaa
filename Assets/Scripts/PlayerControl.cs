﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerControl : MonoBehaviour
{
    // Variables 
    private float horInput;
    private float verInput;
    public float speed = 5.0f;
    public GameObject PrimaryFire;
    public float fireRate = 1;
    public float xRange = 2;
    public float yRange = 1;
    

    // Start is called before the first frame update
    void Start()
    {
        InvokeRepeating("Fire", fireRate, fireRate);
    }

    // Update is called once per frame
    void Update()
    {
        // Get input
        horInput = Input.GetAxis("Horizontal");
        verInput = Input.GetAxis("Vertical");

        // Movement
        transform.Translate(Vector3.up * speed * Time.deltaTime * verInput);
        transform.Translate(Vector3.right * speed * Time.deltaTime * horInput);

        // World Boundaries
        if(transform.position.x < -xRange)
        {
            transform.position = new Vector3(-xRange, transform.position.y, transform.position.z);
        }

        if (transform.position.x > xRange)
        {
            transform.position = new Vector3(xRange, transform.position.y, transform.position.z);
        }

        if (transform.position.y > yRange)
        {
            transform.position = new Vector3(transform.position.x, yRange, transform.position.z);
        }

        if (transform.position.y < -yRange)
        {
            transform.position = new Vector3(transform.position.x, -yRange, transform.position.z);
        }
    }

    void Fire()
    {
        Instantiate(PrimaryFire, transform.position, transform.rotation);
    }
    
}
