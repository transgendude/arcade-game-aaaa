﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StrongEnemyControl : MonoBehaviour
{
    public float fireRate = 1;
    public GameObject EnemyBullet;

    // Start is called before the first frame update
    void Start()
    {
        InvokeRepeating("Shoot", fireRate, fireRate);
    }

    // Update is called once per frame
    void Update()
    {

    }

    void Shoot()
    {
        Instantiate(EnemyBullet, transform.position, transform.rotation);
    }


}
