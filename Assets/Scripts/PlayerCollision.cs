﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCollision : MonoBehaviour
{
    private Lives Health;

    // Start is called before the first frame update
    void Start()
    {
        Health = GameObject.Find("Game Manager").GetComponent<Lives>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    void OnTriggerEnter2D(Collider2D other)
    {
        // Destroy(gameObject);
        Destroy(other.gameObject);
        Lives.Health -= 1;
        
    }
}
