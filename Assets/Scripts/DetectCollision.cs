﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DetectCollision : MonoBehaviour
{
    private Score GetScore;
    public int Value;

    // Start is called before the first frame update
    void Start()
    {
        GetScore = GameObject.Find("Game Manager").GetComponent<Score>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

   
    void OnTriggerEnter2D(Collider2D other)
    {
        Destroy(gameObject);
        Destroy(other.gameObject);
        GetScore.UpdateScore(Value);
    }
}
